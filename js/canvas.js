
  	var canvas = document.getElementById('RecCanvas');
    var ctx = canvas.getContext('2d');

    ctx.fillStyle = 'orange';
    ctx.fillRect(25, 25, 500, 400);
    ctx.clearRect(100, 100, 300, 250);

    ctx.shadowBlur = 20;
	ctx.shadowColor = "black"

    ctx.beginPath();
    ctx.fillStyle = 'blue';
    ctx.moveTo(250,100);
    ctx.lineTo(400, 300);
    ctx.lineTo(100, 300);
    ctx.fill();

    ctx.beginPath();
    ctx.arc(250, 225, 70, 0, Math.PI * 2, true);
    ctx.moveTo(110, 75);
    ctx.fillStyle = 'red';
    ctx.fill();
    ctx.stroke();
